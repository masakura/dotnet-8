﻿# .NET 8
* LTS Release!

## .NET 8
* [Native AOT の改善](./AotApp)
* [.NET 8 あれこれ](./DotNet8)
* [.NET SDK](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#net-sdk)
  - [dotnet restore セキュリティ監査](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#dotnet-restore-security-auditing)
    + 脆弱性情報は [GitHub Advisory Database](https://github.com/advisories) になると思う
    + 未知の脆弱性は置いておいて、既知の脆弱性が登録されていないこともあるので注意
    + CVE 番号が振られたものは掲載されているみたい
    + あと、GitHub Issue を見てるとかなんとか
    + なんにせよ、ないよりましだ!

## C# 12
* [C# 12 あれこれ](./CSharp12)


## .NET MAUI
* [.NET MAUI + Native AOT](./AotMauiApp)


## ASP.NET Core 8