﻿# [.NET MAUI](https://learn.microsoft.com/ja-jp/dotnet/maui/what-is-maui)
.NET MAUI の Windows 以外は Mono で動いている。

![](https://learn.microsoft.com/ja-jp/dotnet/maui/media/what-is-maui/architecture-diagram.png)

iOS だけはさらに AOT が使われている。

> .NET MAUI を使用して構築された iOS アプリは、C# からネイティブ ARM アセンブリ コードに完全に先行してコンパイルされます (AOT)。

.NET 8 から iOS はこの AOT が Mono から .NET に変わるみたい。[Announcing .NET MAUI in .NET 8 Preview 6: Hello VS Code & VS for Mac](https://devblogs.microsoft.com/dotnet/announcing-dotnet-maui-in-dotnet-8-preview-6/) より。

> .NET MAUI is now available in .NET 8 Preview 6 resolving 23 high-impact issues, and introducing Native AOT for iOS.

試しに MAUI Windows アプリで Native AOT を使おうとしたらうまくいかなかったので、MAUI で Native AOT が使えるのは iOS が使えるのは iOS だけなんかも。(Native AOT for Windows は以前から使えてたみたいだし) 
