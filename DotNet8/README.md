﻿## [シリアル化](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#serialization) ([SerializerTest.cs](./SerializerTest.cs))
* [命名ポリシー](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#naming-policies) で `snake_case` に簡単にできるようになった
* [読み取り専用プロパティ](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#read-only-properties) で読み取り専用プロパティの逆シリアル化ができるようになった


## [時間抽象化](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#time-abstraction) ([TimeProviderTest.cs](./TimeProviderTest.cs))
* 時刻や時間に関係あるユニットテストはしんどいんで、似たようなのをよく作ってた
* 今回は、[`Task.Delay()](https://learn.microsoft.com/ja-jp/dotnet/api/system.threading.tasks.task.delay?view=net-8.0) とかに対応させたので、自作のやつより超便利!


## [ランダムに操作する方法](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#methods-for-working-with-randomness) ([RandomTest.cs](./RandomTest.cs))
* ようやくシャッフルが入った!


## [キー付き DI サービス](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#keyed-di-services) ([MicrosoftExtensionsTest.cs](./MicrosoftExtensionsTest.cs))
* DI はいろいろある
  * 古の [EnterpriseLibrary](https://en.wikipedia.org/wiki/Microsoft_Enterprise_Library) (もう誰も覚えてない)
  * [Ninject](http://www.ninject.org/)
  * [Unity](https://github.com/unitycontainer/unity) (あれと違うよ!)
  * [MEF](https://learn.microsoft.com/ja-jp/dotnet/framework/mef/)
  * ...
* 大体、キー付き DI の機能持ってるんだけど、Microsoft.Extensions.DependencyInjection は使えなかった...
* .NET 8 でようやく!
* 属性ベースの DI はまだない (けど、なくてもそれほど困らないかも)


## [パフォーマンスに重点を置いた型](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#performance-focused-types) ([PerformanceTest.cs](./PerformanceTest.cs))
* .NET 8 以前にある `ImmutableDictionary`
  - `IDictionary<K, T>` を実装していないので、`void Foo(IDictionary<K, T>)` の引数に渡せない
  - 要素を追加した新しいインスタンスを返す (元のインスタンスは変わらない)
* .NET 8 で導入された `FrozenDictionary`
  - `IDictionary<K, T>` を実装しているので、`void Foo(IDictionary<K, T)>)` の引数に渡せる 
  - 要素の追加そのものが禁止 (呼び出そうと思えば呼び出せるけど、`NotSupportedException`)