﻿using System.Text.Json;
using System.Text.Json.Serialization;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global

namespace DotNet8;

public sealed class SerializerTest
{
    private static readonly JsonSerializerOptions Options = new()
    {
        WriteIndented = true,
        // JsonNamingPolicy.SnakeCaseLower は .NET 8 から
        PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower
    };

    // .NET 8 以前からできる
    [Fact]
    public void 読み取り専用プロパティのシリアライズ()
    {
        var score = new Score(82, 58, 38);
        var json = JsonSerializer.Serialize(score, Options);

        PAssert.IsTrue(() => json ==
                             """
                             {
                               "japanese": 82,
                               "mathematics": 58,
                               "english": 38,
                               "total": 178,
                               "average": 59
                             }
                             """);
    }

    // .NET 8 から
    [Fact]
    public void 読み取り専用プロパティのデシリアライズ()
    {
        const string json = """
                            {
                              "name": "山田太郎",
                              "score": {
                                "japanese": 82,
                                "mathematics": 58,
                                "english": 38
                              }
                            }
                            """;
        var sample = JsonSerializer.Deserialize<Sample>(json, Options)!;

        var actual = new { sample.Name, sample.Score.Total };

        PAssert.IsTrue(() => sample.Score.Total == 178);
        PAssert.IsTrue(() => sample.Name == "");
    }
}

[JsonObjectCreationHandling(JsonObjectCreationHandling.Populate)]
public sealed class Sample
{
    public string Name { get; } = string.Empty;
    public Score Score { get; } = new(0, 0, 0);
}

public record Score(int Japanese, int Mathematics, int English)
{
    public int Total => Japanese + Mathematics + English;
    public int Average => Total / 3;
}