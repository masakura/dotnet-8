﻿namespace DotNet8;

#if NET8_0_OR_GREATER
public sealed class RandomTest
{
    [Fact]
    public void シャッフル()
    {
        var source = Enumerable.Range(1, 9).ToArray();

        var array = source.ToArray();
        Random.Shared.Shuffle(array);

        PAssert.IsTrue(() => !array.SequenceEqual(source));
    }

    [Fact]
    public void Linqに欲しいなあ()
    {
        var source = Enumerable.Range(1, 9).ToArray();

        var actual = source.Shuffle();
        
        PAssert.IsTrue(() => !actual.SequenceEqual(source));
    }
}

internal static class RandomExtensions
{
    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
    {
        var array = source.ToArray();
        Random.Shared.Shuffle(array);
        return array;
    }
}
#endif