﻿using System.Collections.Frozen;
using System.Collections.Immutable;

namespace DotNet8;

public class PerformanceTest
{
    [Fact]
    public void ImmutableDictionaryTest()
    {
        var original = new Dictionary<string, int>
            {
                { "英語", 32 },
                { "国語", 50 },
            }
            .ToImmutableDictionary();

        var actual = original.Add("数学", 90);
        
        PAssert.IsTrue(() => !actual.SequenceEqual(original));
    }

    [Fact]
    public void ImmutableDictionaryForceAddTest()
    {        var original = new Dictionary<string, int>
        {
            { "英語", 32 },
            { "国語", 50 },
        }
        .ToImmutableDictionary();
    }
    
    [Fact]
    public void FrozenDictionaryTest()
    {
        var original = new Dictionary<string, int>
            {
                { "英語", 32 },
                { "国語", 50 },
            }
            .ToFrozenDictionary();

        // Add() メソッドなどの更新系が使えないようになっている
        // var actual = original.Add("数学", 90);
    }
}