﻿namespace DotNet8;

public sealed class TimeProviderTest
{
    private readonly MockTimeProvider _timeProvider = new();

    [Fact]
    public void 現在の時刻をごまかす()
    {
        var now = DateTimeOffset.Parse("2023/10/10 12:00:00 +09:00");

        _timeProvider.SetNow(now);

        var actual = _timeProvider.GetLocalNow();

        PAssert.IsTrue(() => now == actual);
    }

#if NET8_0_OR_GREATER
    [Fact]
    public async Task すぐに終わるようにする()
    {
        await WaitOneMinuteAsync(_timeProvider);
    }

    private async Task WaitOneMinuteAsync(TimeProvider timeProvider)
    {
        await Task.Delay(TimeSpan.FromMinutes(1), timeProvider);
    }
#endif
}

internal sealed class MockTimeProvider : TimeProvider
{
    private DateTimeOffset _utcNow;

    public MockTimeProvider()
    {
        _utcNow = base.GetUtcNow();
    }

    public void SetNow(DateTimeOffset now)
    {
        _utcNow = now.ToUniversalTime();
    }

    public override DateTimeOffset GetUtcNow()
    {
        return _utcNow;
    }

    public override ITimer CreateTimer(TimerCallback callback, object? state, TimeSpan dueTime, TimeSpan period)
    {
        var timer = new Timer(callback, state, TimeSpan.Zero, TimeSpan.Zero);

#if NET8_0_OR_GREATER
        // 即座に終了するタイマー
        return timer;
#else
        // .NET 8 以前 (このやり方がいいのかわからない)
        return new TimerAdapter(timer);
#endif
    }
}

internal class TimerAdapter(Timer timer) : ITimer
{
    public void Dispose()
    {
        timer.Dispose();
    }

    public ValueTask DisposeAsync()
    {
        return timer.DisposeAsync();
    }

    public bool Change(TimeSpan dueTime, TimeSpan period)
    {
        return timer.Change(dueTime, period);
    }
}