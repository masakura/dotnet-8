﻿using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace DotNet8;

public sealed class MicrosoftExtensionsTest : IDisposable
{
    private readonly IServiceScope _scope;
    private readonly ServiceProvider _services;

    public MicrosoftExtensionsTest()
    {
        _services = new ServiceCollection()
            .AddKeyedScoped<IFilter, Filter1>("heavy")
            .AddKeyedScoped<IFilter, Filter2>("light")
            .AddKeyedScoped<IFilter, Filter3>("light")
            .BuildServiceProvider();

        _scope = _services.CreateScope();
    }

    public void Dispose()
    {
        _scope.Dispose();
        _services.Dispose();
    }

    [Fact]
    public void 重いやつを実行()
    {
        var filters = _scope.ServiceProvider.GetKeyedServices<IFilter>("heavy");

        var actual = filters.Output();

        PAssert.IsTrue(() => actual == "Filter1");
    }

    [Fact]
    public void 軽いやつを実行()
    {
        var filters = _scope.ServiceProvider.GetKeyedServices<IFilter>("light");

        var actual = filters.Output();

        // 本当は順序は保証されていないはず
        PAssert.IsTrue(() => actual ==
                             """
                             Filter2
                             Filter3
                             """);
    }
}

public class Filter1 : IFilter;

public class Filter2 : IFilter;

public class Filter3 : IFilter;

public interface IFilter
{
    public void Output(StringBuilder builder)
    {
        builder.AppendLine(GetType().Name);
    }
}

internal static class FilterExtensions
{
    public static string Output(this IEnumerable<IFilter> filters)
    {
        var builder = new StringBuilder();
        foreach (var filter in filters) filter.Output(builder);

        return builder.ToString().Trim();
    }
}