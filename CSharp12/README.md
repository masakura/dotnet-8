﻿# C# 12.0
なんかめぼしいものがない...


## [プライマリコンストラクター](https://learn.microsoft.com/ja-jp/dotnet/csharp/whats-new/csharp-12#primary-constructors) ([PrimaryConstructorTest.cs](./PrimaryConstructor))
* 短く書くときに便利! (Kotlin に追いついた!)


## [コレクション式](https://learn.microsoft.com/ja-jp/dotnet/csharp/whats-new/csharp-12#collection-expressions) ([コレクション式.cs](./コレクション式.cs))
* コレクションだけだけど、JavaScript っぽく書けるようになった!


## [インライン配列](https://learn.microsoft.com/ja-jp/dotnet/csharp/whats-new/csharp-12#inline-arrays) ([InlineArrayTest.cs](./InlineArrayTest.cs))
* 一部のフレームワークの高速化用
* 固定長の構造体の配列をスタック上に確保する
* 通常はヒープに配列が確保され、その配列の中に構造体の値が入っている

```
 | |
 +-+
 |2|
 +-+
 |1|
 +-+
 |2|
 +-+
 |1| <--- other
 +-+
 |2|
 +-+
 |1|
 +-+
 |2|
 +-+
 |1| <--- original
-+-+-
 | |
```

普通の配列はヒープに確保される (あくまで雰囲気)

```
 |     |                 |address|value|
 +-----+                 |-------|-----|
 |@1234| <--- other      | @1232 |     |
 +-----+                 | @1233 |     |
 |@1234| <--- original   | @1234 |  8  | <- capacity
-+-----+-                | @1235 |  4  | <- length
 |     |                 | @1236 |  1  |
                         | @1237 |  2  |
                         | @1238 |  1  |
                         | @1239 |  2  |
                         | @123a |     |
```


## [インターセプター](https://learn.microsoft.com/ja-jp/dotnet/csharp/whats-new/csharp-12#primary-constructors)
TODO やってはみたいけど、ソースジェネレーターを覚えんといかんっぽい。