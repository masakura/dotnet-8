﻿using System.Collections.Immutable;

namespace CSharp12;

public sealed class コレクション式
{
    [Fact]
    public void 宣言()
    {
#if NET8_0_OR_GREATER
        ImmutableArray<string> fruits = ["apple", "orange"];
#else
        // .NET 7 だと ImmutableArray<string> はだめだった... なんでかは知らん
        string[] fruits = ["apple", "orange"];
#endif
        // var fruits2 = ["apple", "orange"]; // これはだめ
        var fruits3 = new[] { "apple", "orange" }; // 今までで一番短い書き方
    }

    [Fact]
    public void スプレッド演算子()
    {
        string[] fruits1 = ["apple", "orange"];
        string[] fruits2 = ["banana"];

        string[] fruits3 = [..fruits1, ..fruits2];
        
        PAssert.IsTrue(() => fruits3.SequenceEqual(new[]
        {
            "apple",
            "orange",
            "banana"
        }));
    }
}