﻿using System.Runtime.CompilerServices;

namespace CSharp12;

#if NET8_0_OR_GREATER
public sealed class InlineArrayTest
{
    [Fact]
    public void インライン配列()
    {
        var board = new ゲーム盤();
        board[0] = 駒.表;
        board[1] = 駒.裏;
        board[2] = 駒.表;
        board[3] = 駒.裏;

        var actual = board.ToEnumerable();
        
        PAssert.IsTrue(() => actual.SequenceEqual(
            new[] { 駒.表, 駒.裏, 駒.表, 駒.裏 }
        ));
    }

    [Fact]
    public void スタックが使われる()
    {
        var original = new ゲーム盤();
        original[0] = 駒.表;
        original[1] = 駒.裏;
        original[2] = 駒.表;
        original[3] = 駒.裏;

        var other = original;
        other[0] = 駒.裏;
        
        // スタックが使われるので、
        // `original` と `other` は別々のメモリにある
        PAssert.IsTrue(() => !other.ToEnumerable()
            .SequenceEqual(original.ToEnumerable()));
    }
    
    [Fact]
    public void 非インライン配列()
    {
        var original = new 駒[4];
        original[0] = 駒.表;
        original[1] = 駒.裏;
        original[2] = 駒.表;
        original[3] = 駒.裏;

        var other = original;
        other[0] = 駒.裏;
        
        // 配列はヒープに配置されるので、
        // `original` と `other` は同じインスタンスを指す
        PAssert.IsTrue(() => other.SequenceEqual(original));
    }
}

[InlineArray(4)]
public struct ゲーム盤
{
    private 駒 駒;

    public IEnumerable<駒> ToEnumerable()
    {
        foreach (var element in this)
        {
            yield return element;
        }
    }
}

public readonly record struct 駒(int State)
{
    public static 駒 表 { get; } = new(1);
    public static 駒 裏 { get; } = new(2);

    public override string ToString()
    {
        return State switch
        {
            1 => "表",
            2 => "裏",
            _ => throw new NotSupportedException()
        };
    }
}
#endif