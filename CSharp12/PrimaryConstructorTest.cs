﻿// ReSharper disable ArrangeTypeModifiers
// ReSharper disable UnusedType.Global
// ReSharper disable NotAccessedPositionalProperty.Global
namespace CSharp12;

// .NET 8 より前から OK
record Class1(int Value);

// .NET 8 から許容
class Class2(int Value);

record struct Struct1(int Value);

struct Struct2(int Value);