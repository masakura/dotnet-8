﻿// See https://aka.ms/new-console-template for more information

using System.Text.Json;
using AotApp;

Console.WriteLine("Hello, World!");

const string json = """
                    {"name":"taro"}
                    """;

var person = JsonSerializer.Deserialize<Person>(json, new JsonSerializerOptions
{
    TypeInfoResolver = SourceGenerationContext.Default,
    PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower
});
Console.WriteLine(person);