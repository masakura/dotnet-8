﻿# [Native AOT](https://learn.microsoft.com/ja-jp/dotnet/core/whats-new/dotnet-8#native-aot-support)
* Windows / Linux に加えて macOS / iOS 系が追加された模様 https://learn.microsoft.com/ja-jp/dotnet/core/deploying/native-aot/?tabs=net8plus%2Cwindows#platformarchitecture-restrictions
* クロスコンパイルはできないみたい (Go 言語がうらやましい)

```dotnet
$ cd AotApp
$ dotnet -c Release
```

GitLab CI でビルドしたやつ。

| Runtime   | download                                                                                                                                             |
|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| linux-x64 | [AotApp](https://gitlab.com/api/v4/projects/51409450/jobs/artifacts/master/raw/AotApp/bin/Release/net8.0/linux-x64/publish/AotApp?job=aot:linux-x64) |
| macos-arm | [AotApp](https://gitlab.com/api/v4/projects/51409450/jobs/artifacts/master/raw/AotApp/bin/Release/net8.0/osx-arm64/publish/AotApp?job=aot:osx-arm64) |
| win-x64   | ~~AotApp.exe~~ (GitLab SaaS Runner ではビルドできず)                                                                                                         |
