﻿using System.Text.Json.Serialization;

namespace AotApp;

public record Person(string Name);

[JsonSourceGenerationOptions(WriteIndented = true)]
[JsonSerializable(typeof(Person))]
[JsonSerializable(typeof(string))]
// ReSharper disable once PartialTypeWithSinglePart
// ReSharper disable once UnusedType.Global
internal partial class SourceGenerationContext : JsonSerializerContext;